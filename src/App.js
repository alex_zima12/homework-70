import React from 'react';
import DishesList from "./components/DishesList/DishesList";

function App() {
  return (
      <div className="container d-flex flex-wrap justify-content-between">
    <DishesList/>
      </div>
  );
}

export default App;
