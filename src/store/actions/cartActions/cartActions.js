import {
    ADD_DISH,
    REMOVE_DISH,
    SET_PURCHASING,
    INIT_INGREDIENTS
} from "../actionTypes";

export const addDish = (indexItem, nameItem, priceItem, countItem) => {
    return {type: ADD_DISH, indexItem, nameItem, priceItem, countItem};
};
export const removeDish= (index, priceItem) => {
    return {type: REMOVE_DISH, index, priceItem};
};

export const setPurchasing = purchasing => {
    return {type: SET_PURCHASING, purchasing: purchasing}
};

export const initIngredients = () => {
    return {type: INIT_INGREDIENTS};
};



