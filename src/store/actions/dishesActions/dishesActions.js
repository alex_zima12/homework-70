import {
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS
} from "../actionTypes";

import axiosRestoran from "../../../axiosRestoran";

const fetchDishesRequest = () => {
    return {type: FETCH_DISHES_REQUEST};
};
const fetchDishesSuccess = menu => {
    return {type: FETCH_DISHES_SUCCESS, menu};
};
const fetchDishesFailure = error => {
    return {type: FETCH_DISHES_FAILURE, error};
};

export const fetchDishes = () => {
    return async dispatch => {
        dispatch(fetchDishesRequest());
        try {
            const response = await axiosRestoran.get('/dishes.json');
            dispatch(fetchDishesSuccess(response.data));
        } catch(e) {
            dispatch(fetchDishesFailure(e));
        }

    };
};

