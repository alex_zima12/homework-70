import {
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS
} from "../actionTypes";

import axiosRestoran from "../../../axiosRestoran";

const orderRequest = () => {
    return {type: ORDER_REQUEST};
};
const orderSuccess = () => {
    return {type: ORDER_SUCCESS};
};
const orderFailure = error => {
    return {type: ORDER_FAILURE, error};
};

export const createOrder = order => {
    return async dispatch => {
        dispatch(orderRequest());
        try {
            const response = await axiosRestoran.post("/pizzaOrders.json", order);
            dispatch(orderSuccess(response.data));
        } catch(e) {
            dispatch(orderFailure(e));
        }
    };
};
