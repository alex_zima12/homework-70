import {ADD_DISH, REMOVE_DISH, SET_PURCHASING, INIT_INGREDIENTS} from "../actions/actionTypes";

const initialState = {
    cart: [],
    totalPrice: 0,
    purchasing: false
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH:
            const index = state.cart.findIndex(obj => obj.index === action.indexItem)
            if(index !== -1) {
                const copyStateCart = [...state.cart];
                const copyObj = {...copyStateCart[index]};
                copyObj.count += 1;
                copyStateCart[index] = copyObj;
                return {...state, cart: [...copyStateCart],totalPrice: state.totalPrice + action.priceItem}

            }else {
                return {
                    ...state,
                    cart: [...state.cart,
                {
                    name: action.nameItem,
                    index: action.indexItem,
                    price: action.priceItem,
                    count: 1
                }],totalPrice: state.totalPrice + action.priceItem
                }
            }

        case REMOVE_DISH:
            const copyStateCart = [...state.cart];
            copyStateCart.splice(action.index, 1);
            return {
                ...state,
                cart: [...copyStateCart],
                totalPrice: state.totalPrice - action.priceItem
            }
        case SET_PURCHASING:
            return {
                ...state,
                purchasing: action.purchasing
            };
        case INIT_INGREDIENTS:
            return {
                ...state,
                cart: [],
                totalPrice: 0
            };
        default:
            return state;
    }
};

export default cartReducer;