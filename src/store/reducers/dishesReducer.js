import {
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    FETCH_DISHES_FAILURE,
} from "../actions/actionTypes";

const initialState = {
    loading: false,
    error: null,
    fetchError: null,
    dishes: [],
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state,
                loading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state,
                loading: false,
                dishes: action.menu};
        case FETCH_DISHES_FAILURE:
            return {...state,
                loading: false,
                fetchError: action.error};
        default:
            return state;
    }
};

export default dishesReducer;