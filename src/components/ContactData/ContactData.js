import React, {useState} from 'react';
import {useSelector, useDispatch} from "react-redux";
import Spinner from "../UI/Spinner/Spinner";
import {createOrder} from "../../store/actions/orderActions/orderActions";

const ContactData = (props) => {
    const [customer, setCustomer] = useState({
        name: '',
        email: '',
        street: '',
        postal: ''
    });

    const cart = useSelector(state => state.cart.cart);
    const loading = useSelector(state => state.dishes.loading);
    const dispatch = useDispatch();

    const customerDataChanged = event => {
        const name = event.target.name; // email
        const value = event.target.value;
        setCustomer(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const orderHandler = async event => {
        event.preventDefault();
       const order = cart.map(el =>  {
           return {
               dish: el.name,
               dishCount: el.count,
               customer: {...customer}
           }
        });
        dispatch(createOrder(order));
    };

    let form = (
        <form onSubmit={orderHandler}>
            <div className="form-group">
                <label>Name</label>
                <input
                    className="form-control" placeholder="Your Name"
                    type="text" name="name"
                    value={customer.name}
                    onChange={customerDataChanged}
                    required
                />
            </div>
            <div className="form-group">
                <label>Email address</label>
                <input
                    className="form-control" placeholder="Your Mail"
                    type="email" name="email"
                    value={customer.email}
                    onChange={customerDataChanged}
                    required
                />
                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone
                    else.</small>
            </div>

            <div className="form-group">
                <label>Address</label>
                <input
                    className="form-control" placeholder="Street"
                    type="text" name="street"
                    value={customer.street}
                    onChange={customerDataChanged}
                    required
                />
            </div>

            <div className="form-group">
                <label>Postal Code</label>
                <input
                    className="form-control" placeholder="Postal Code"
                    type="text" name="postal"
                    value={customer.postal}
                    onChange={customerDataChanged}
                    required
                />
            </div>
            <button
                className="btn btn-primary">ORDER
            </button>
            <button
                className="btn btn-danger ml-3"
                onClick={props.purchaseCancelled}
            >CANCEL
            </button>
        </form>
    );

    if (loading) {
        form = <Spinner/>;
    }

    return (
        <div className="ContactData">
            <h4>Enter your Contact Data</h4>
            {form}
        </div>
    );
};
export default ContactData;
