import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "../../store/actions/dishesActions/dishesActions";
import Spinner from "../UI/Spinner/Spinner";
import MenuItem from "../MenuItem/MenuItem";
import Order from "../Order/Order";
import Counter from "../Counter/Counter";
import {addDish, setPurchasing,initIngredients} from "../../store/actions/cartActions/cartActions";
import ContactData from "../ContactData/ContactData";
import Modal from "../UI/Modal/Modal";

const DishesList = () => {
    const dishes = useSelector(state => state.dishes.dishes);
    const loading = useSelector(state => state.dishes.loading);
    const purchasing = useSelector(state => state.cart.purchasing);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    let dishesOutput = Object.values(dishes).map((dish, index) => {
        return (<MenuItem
            name={dish.name}
            price={dish.price}
            img={dish.img}
            key={dish.name}
            keyIndex={index}
            count={0}
            addItemToOrder={(indexItem, nameItem, priceItem, countItem) => {
                addDishHandler(indexItem, nameItem, priceItem, countItem);
            }}
        />)
    });

    const addDishHandler = (indexItem, nameItem, priceItem, countItem) => {
        dispatch(addDish(indexItem, nameItem, priceItem, countItem));
    };

    const purchaseCancelHandler = () => {
        dispatch(setPurchasing(false));
        dispatch(initIngredients());
    };
    if (loading) {
        dishesOutput = <Spinner/>;
    }
    return (
        <>
            {dishesOutput}
            <div className="container">
                <h1>Your order: </h1>
                <Order/>
                <Counter/>
                <Modal show={purchasing} closed={purchaseCancelHandler}>
                <ContactData
                    purchaseCancelled={purchaseCancelHandler}
                />
                </Modal>
            </div>
        </>
    );
};

export default DishesList;