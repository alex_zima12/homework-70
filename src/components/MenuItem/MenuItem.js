import React from 'react';

const MenuItem = props => {

       return (
         <div className="card w-25 d-flex align-items-md-center p-3">
                <img src={props.img} className="card-img-top " alt="dish" style={{width:200}}/>
                <div className="card-body d-flex flex-column">
                    <h5 className="card-title"> Name: {props.name}</h5>
                    <p className="card-text"> Price: {props.price} KGS</p>
                </div>
              <button className="btn btn-primary m-auto pb-3" type="submit" onClick={() => props.addItemToOrder(props.keyIndex,props.name,props.price,props.count)}>Add to cart</button>
        </div>

          );
};

export default MenuItem;