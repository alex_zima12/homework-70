import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setPurchasing} from "../../store/actions/cartActions/cartActions";

const Counter = () => {
    let total = useSelector(state => state.cart.totalPrice);
    const dispatch = useDispatch();

    const purchaseOpenHandler = () => {
        dispatch(setPurchasing(true));
    };

    let btn;
    if (total > 0) {
        btn = <button type="button" className="btn btn-success" onClick={purchaseOpenHandler}>Place Order</button>
    }
   return (
       <>
        <div className="d-flex flex-row-reverse mt-3">
            Total price: {total}
        </div>
           <div className='d-flex flex-row-reverse mt-3'>{btn}</div>
           </>
    );
};

export default Counter;