import React from 'react';

const OrderItem = (props) => {

    return (
        <li className="list-group-item">
            <p>{props.name}</p>
            <p>{props.count}</p>
            <p>{props.price}</p>
            <button className="btn btn-danger " onClick={() => props.deleteItemFromOrder(props.keyIndex)}>Delete</button>
        </li>
    );
};

export default OrderItem;

