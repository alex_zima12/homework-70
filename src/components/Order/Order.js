import React from 'react';
import OrderItem from "../OrderItem/OrderItem";
import {useDispatch, useSelector} from "react-redux";
import {removeDish} from "../../store/actions/cartActions/cartActions";

const Order = () => {
    const cart = useSelector(state => state.cart.cart);
    const dispatch = useDispatch();

    const removeDishHandler = (index,priceItem) => {
        dispatch(removeDish(index,priceItem));
    };

    let arr;
    if (cart) {
        arr = [];
        Object.values(cart).forEach((item, index) => {
            if (item.count > 0) {
                arr.push(<OrderItem
                    key={index}
                    keyIndex={index}
                    name={item.name}
                    count={item.count}
                    price={item.price}
                    deleteItemFromOrder={() => removeDishHandler(index, item.price)}
                />)
            }
        });
    }

    return (
        <ul className="list-group">
            {arr}
        </ul>
    );
};

export default Order;