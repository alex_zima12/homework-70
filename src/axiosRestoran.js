import axios from 'axios';

const axiosRestoran = axios.create({
    baseURL: 'https://jsgroup7exam.firebaseio.com/'
});

export default axiosRestoran;